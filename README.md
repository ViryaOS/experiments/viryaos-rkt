# viryaos-rkt for v3.7

This directory has two docker files which is used to build and create an image
of `viryaos-rkt`.

```
$ cd viyaos-rkt

$ cp /usr/bin/qemu-aarch64-static .
$ docker build --squash -t viryaos-rkt-builder-aarch64 .
```

Once the image is built, running the image with `run` script, would generate
`viryaos-rkt.tar.gz` in `/tmp` directory.

```
$ cd viyaos-rkt

$ docker run --rm -ti -v /tmp:/tmp -v $(pwd):/root/src \
    viryaos-rkt-builder-aarch64 /root/src/run
```

`viryaos-rkt.tar.gz` contains the rkt build for `aarch64`.

Once `viryaos-rkt.tar.gz` is built, it can be packaged as a docker image for
downstream use. This can be done using `Dockerfile.image`.

```
(inside viryaos-rkt directory)

$ git rev-parse --short HEAD
```

To build the docker image

```
$ cd viryaos-rkt

$ cp /tmp/viryaos-rkt.tar.gz .

$ docker build -f Dockerfile.image --squash \
    -t viryaos/viryaos-rkt:<VERSION>-<SHORT_SHA>-<ARCH> .

(For example)

$ docker build -f Dockerfile.image --squash \
    -t viryaos/viryaos-rkt:v3.7-abcdef1-aarch64 .
```
